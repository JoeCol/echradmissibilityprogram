QuestionID;NodeID;NextIfTrue;NextIfFalse;endReason
Q1;F1F1F1F1Q1;Q2;Q10; 
Q2;F1F1F1F1F1Q4;Q3;Q9; 
Q3;F1F1F1F1F1Q3;Q4;Q7; 
Q4;F1F1F1F1F1Q2;Q5;Q6; 
Q5;F1F1F1F1F1Q1;Q19;Q19; 
Q6;F1F1F1F1F1Q1;Q19;NULL;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. You or your legal representative will be required to sign your application. 
Q7;F1F1F1F1F1Q1;NULL;Q8;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. You will be required to sign the power of attorney.
Q8;F1F1F1F1F1Q2;Q19;NULL;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. You or your legal representative will be required to sign your application.
Q9;F1F1F1F1F1Q3;Q19;NULL;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. You or your legal representative will be required to sign your application.
Q10;F1F1F1F2Q1;Q11;NULL;Your application will be inadmissible because according to Article 34 of the European Convention the Court accepts individual applications from any person; non-governmental organisation or group of persons.
Q11;F1F1F1F2Q2;Q12;NULL;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. You will be required to fill out page 4 of your application. 
Q12;F1F1F1F2F1Q1;Q14;Q13; 
Q13;F1F1F1F2Q3;Q19;NULL;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. Your director will be required to sign the application. 
Q14;F1F1F1F2F1Q2;Q17;Q15; 
Q15;F1F1F1F2F1Q3;NULL;Q16;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. Your director will be required to sign your application. 
Q16;F1F1F1F2Q3;Q19;NULL;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. Your director will be required to sign the application.  
Q17;F1F1F1F2Q3;Q19;Q18; 
Q18;F1F1F1F2F1Q3;Q19;NULL;Your application will not be registered because it does not comply with Rule 47 of the Rules of Court. Your director or legal representative will be required to sign the application. 
Q19;F1F1F2Q1;Q20;Q21; 
Q20;F1F1F2Q3;NULL;Q25;Your application will be declared inadmissible because only victims of alleged violations can apply to the European Court. 
Q21;F1F1F2Q2;Q22;Q23; 
Q22;F1F1F2Q3;NULL;Q25;Your application will be declared inadmissible because only victims of alleged violations can apply to the European Court. 
Q23;F1F1F2Q4;Q25;NULL;Your application will be declared inadmissible because only victims of alleged violations can apply to the European Court. 
Q24;F1F1F2Q3;NULL;Q25;Your application will be declared inadmissible because only victims of alleged violations can apply to the European Court.  
Q25;F2F1F1F1Q1;Q26;Q30; 
Q26;F2F1F1Q1;Q27;Q28; 
Q27;F2F1F1Q2;Q30;NULL;If the applicant did not make any attempts to complain about their rights violation in domestic proceedings then their application will be declared inadmissible.
Q28;F2F1F1F1Q1;Q29;Q30; 
Q29;F2F1F1Q3;Q30;NULL;If the applicant did not make a reasonable attempt to exhaust domestic remedies then the Court will declare such application inadmissible. 
Q30;F2F2Q1;Q35;Q31; 
Q31;F2F2F1Q2;Q32;Q33; 
Q32;F2F2F1Q1;NULL;NULL;[You need to complain to the national authorities first and if unsuccessful then bring your claim to the European Court. At the moment your application is inadmissible.:Your application will be declared inadmissible as it was not submitted with the 6 months period.] 
Q33;F2F2F1Q1;Q35;Q34; 
Q34;F2F2F1Q3;Q35;NULL;Your application will be declared inadmissible as it was not submitted with the 6 months period.  
Q35;F2F3Q1;Q36;NULL;Your application is inadmissible because the application form should contain information about the applicant.  
Q36;F2F4F1Q1;Q37;Q39; 
Q37;F2F4F1Q2;Q38;Q39; 
Q38;F2F4F1Q3;Q39;NULL;Your application will be declared inadmissible due to the fact that you cannot submit substantively the same applications twice.  
Q39;F2F4F2Q1;Q40;Q42; 
Q40;F2F4F2Q2;Q41;Q42; 
Q41;F2F4F2Q3;NULL;Q42;Your application will be declared inadmissible because it was submitted to another international body.
Q42;F2F5F1Q1;NULL;Q43;The application is inadmissible for abuse of the right of petition 
Q43;F2F5F1Q2;NULL;Q44;The application is inadmissible for abuse of the right of petition 
Q44;F2F5F1Q3;NULL;Q45;The application is inadmissible for abuse of the right of petition 
Q45;F2F5F1Q4;NULL;Q46;The application is inadmissible for abuse of the right of petition 
Q46;F2F5F2Q1;Q47;Q48; 
Q47;F2F5F2Q2;NULL;Q48;The application is inadmissible for abuse of the right of petition 
Q48;F2F5F4Q1;NULL;Q49;It is highly likely that the Court will declare your new application manifestly ill-founded too.
Q49;F3F1Q1;Q50;NULL;The application is inadmissible the European Court of Human Rights can only consider alleged violation committed by the Contracting Parties to the European Convention on Human Rights.
Q50;F3F1Q2;Q51;NULL;Your application might not be registered as it does not comply with rule 47 of the Rules of Court. 
Q51;F3F1Q3;Q52;Q53; 
Q52;F3F1Q4;Q53;NULL;Your application will be declared inadmissible as the Court can only review cases of violation of rights ratified by the Contracting Parties.
Q53;F3F2Q2;Q54;Q55; 
Q54;F3F2Q1;Q60;NULL;Your application might not be registered as it does not comply with rule 47 of the Rules of Court.
Q55;F3F2Q3;Q56;Q57; 
Q56;F3F2Q1;Q60;NULL;Your application might not be registered as it does not comply with rule 47 of the Rules of Court.
Q57;F3F2Q4;Q58;Q59; 
Q58;F3F2Q1;Q60;NULL;Your application might not be registered as it does not comply with rule 47 of the Rules of Court.
Q59;F3F2Q5;Q60;NULL;Your application is likely to be declared inadmissible. The violation must occur under the jurisdiction of one of the Contracting Parties to the Convention
Q60;F4F1Q1;Q61;Q63; 
Q61;F4F1Q2;Q62;NULL;It is likely that your application will be declared inadmissible.
Q62;F4F1Q3;Q63;NULL;It is likely that your application will be declared inadmissible. 
Q63;F4F2F1Q1;NULL;Q64;It is likely that your application will be declared inadmissible.
Q64;F4F2F1Q2;Q65;NULL;It is likely that your application will be declared inadmissible.
Q65;F4F2F2Q1;Q67;Q66; 
Q66;F4F2F2Q2;Q67;NULL;It is likely that your application will be declared inadmissible. 
Q67;F4F2F3Q1;Q68;NULL;It is likely that your application form will not be registered due to its failure to comply with Rule 47 of the Rules of Court.
Q68;F4F2F3F1Q1;Q69;NULL;It is likely that your application form will not be registered due to its failure to comply with Rule 47 of the Rules of Court.
Q69;F4F2F3F1F1Q1;Q71;Q70; 
Q70;F4F2F3F1F1Q2;Q71;NULL;It is likely that your application will be declared inadmissible.
Q71;F4F2Q1;FINISH;Q72;It is possible that your application will be declared admissible. 
Q72;F4F2Q2;FINISH;Q73;It is possible that your application will be declared admissible. 
Q73;F4F2F1Q2;Q74;NULL;It is possible that your application will be declared admissible.
Q74;F4F2F1Q1;FINISH;NULL;[It is possible that your application will be declared admissible:It is likely that your application will be declared inadmissible.] 