import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

public class CheckBoxListener implements ActionListener 
{
	private ADFNode reference;
	public CheckBoxListener(ADFNode ref)
	{
		reference = ref;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		reference.setValid(((JCheckBox)(arg0.getSource())).isSelected());

	}

}
