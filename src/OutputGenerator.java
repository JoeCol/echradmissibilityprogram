import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;


public class OutputGenerator 
{
	static ADFTree t = new ADFTree();
	
	static ArrayList<boolean[]> runs = new ArrayList<boolean[]>();
	static TreeSet<String> validRuns = new TreeSet<String>();
	//static TreeSet<PossibleOutput> possibleOutputs = new TreeSet<PossibleOutput>();
	static ArrayList<Question> allQuestions;
	
	/*public static void main(String[] args) throws IOException 
	{
		t.loadADF();
		allQuestions = t.getQuestions();
		addRuns(0, "");
		Iterator<String> it = validRuns.iterator();
		
		while (it.hasNext()) 
		{
			String i = it.next();
		    System.out.println(i);
		    String[] parts = i.split(",");

		    boolean[] array = new boolean[parts.length];
		    for (int j = 0; j < parts.length; j++)
		    {
		        array[j] = Boolean.parseBoolean(parts[j]);
		    }
		    String s = doTest(array);
		    possibleOutputs.add(new PossibleOutput(i, s));
		}
		Iterator<PossibleOutput> poi = possibleOutputs.iterator();
		FileWriter fw = new FileWriter("AllRuns.csv");
		while (poi.hasNext())
		{
			PossibleOutput po = poi.next();
			fw.write("\"" + po.explanation + "\",\"" + po.run + "\"");
		    fw.write(System.lineSeparator());
		}
		fw.close();
		System.out.println(validRuns.size() + " valid inputs");
	}*/
	
	public static void main(String[] args) throws IOException 
	{
		t.loadADF();
		allQuestions = t.getQuestions();
		
		if (args.length > 0)
		{
			ArrayList<Boolean> answers = stringListToBoolArr(args[0]);
			ADFTree.QuestionState lq = ADFTree.QuestionState.qsAsking;
			for (int i = 0; i < answers.size(); i++)
			{
				 lq = t.goToNextQuestion(answers.get(i));
			}
			System.out.println(t.getMainResult());
			System.out.println(t.getReasoning(lq == ADFTree.QuestionState.qtAccepted));
		}
		else
		{
			FileWriter fw = new FileWriter("IncorrectRuns.txt");	
			printRuns(0,new ArrayList<Boolean>(), fw, 0);		
			fw.close();
			/*fw = new FileWriter("AllRuns.csv");
			Iterator<PossibleOutput> poi = possibleOutputs.iterator();
			while (poi.hasNext())
			{
				PossibleOutput po = poi.next();
				fw.write("\"" + po.explanation + "\",\"" + po.run + "\"");
			    fw.write(System.lineSeparator());
			}
			fw.close();*/
			System.out.println(validRuns.size() + " valid inputs");
		}
	}
	
	public static void printRuns(int index, ArrayList<Boolean> path, FileWriter fw, int depth)
	{
		if (depth > path.size())
		{
			System.out.println("Depth greater than size");
		}
		Question q = allQuestions.get(index);
		Boolean[] testPath;
		path.add(true);
		testPath = new Boolean[path.size()];
		testPath = path.toArray(testPath);
		if (q.getTrueNextQuestionID().equalsIgnoreCase("NULL"))
		{
			doTest(testPath, fw);			
		}
		else if (q.getTrueNextQuestionID().equalsIgnoreCase("FINISH"))
		{
			
			doTest(testPath, fw);	
		}
		else
		{
			printRuns(t.findQuestionIndexFromID(q.getTrueNextQuestionID()), path, fw, depth + 1);
		}
		
		path.remove(path.size() - 1);
		path.add(false);
		testPath = new Boolean[path.size()];
		testPath = path.toArray(testPath);
		if (q.getFalseNextQuestionID().equalsIgnoreCase("NULL"))
		{
			doTest(testPath, fw);	
		}
		else if (q.getFalseNextQuestionID().equalsIgnoreCase("FINISH"))
		{
			doTest(testPath, fw);	
		}
		else
		{		
			printRuns(t.findQuestionIndexFromID(q.getFalseNextQuestionID()), path, fw, depth + 1);
		}
		
		path.remove(path.size() - 1);
	}
	
	public static String boolArrToStringList(Boolean[] arr)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i != arr.length - 1)
			{
				sb.append(",");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	public static ArrayList<Boolean> stringListToBoolArr(String list)
	{
		String[] splitList = list.replace("[", "").replace("]", "").split(",");
		ArrayList<Boolean> bools = new ArrayList<Boolean>();
		for (int i = 0; i < splitList.length; i++)
		{
			bools.add(Boolean.valueOf(splitList[i]));
		}
		return bools;
	}
	
	public static void addRuns(int index, String path)
	{
		Question q = allQuestions.get(index);
		
		if (q.getTrueNextQuestionID().equalsIgnoreCase("NULL"))
		{
			validRuns.add(path + "true");
		}
		else if (q.getTrueNextQuestionID().equalsIgnoreCase("FINISH"))
		{
			return;
		}
		else
		{
			addRuns(t.findQuestionIndexFromID(q.getTrueNextQuestionID()), path + "true,");
		}
		
		if (q.getFalseNextQuestionID().equalsIgnoreCase("NULL"))
		{
			validRuns.add(path + "false");
			
		}
		else if (q.getFalseNextQuestionID().equalsIgnoreCase("FINISH"))
		{
			return;
		}
		else
		{
			addRuns(t.findQuestionIndexFromID(q.getFalseNextQuestionID()), path + "false,");
		}
	}
	
	public static String doTest(Boolean[] test, FileWriter fw)
	{
		t.resetQuestions();
		ADFTree.QuestionState qs = ADFTree.QuestionState.qsRejected;
		for (int i = 0; i < test.length; i++)
		{
			qs = t.goToNextQuestion(test[i]);
			if (qs != ADFTree.QuestionState.qsAsking && (i == test.length - 1))
			{
				System.out.println("Run Completed");
				break;
			}
			else if (qs != ADFTree.QuestionState.qsAsking && (i != test.length - 1))
			{
				System.out.println("Error in run");
			}
		}
		String toReturn = t.getReasoning(qs == ADFTree.QuestionState.qtAccepted);
		//PossibleOutput po = new PossibleOutput(boolArrToStringList(test), toReturn);
		//possibleOutputs.add(po);
		if (toReturn.contains("recommended not to submit application") && qs == ADFTree.QuestionState.qtAccepted)
		{
			System.out.println(boolArrToStringList(test));
			System.out.println("Rejecting a valid test");
			try {
				fw.write(boolArrToStringList(test) + System.lineSeparator());
				fw.write("Rejecting a valid test" + System.lineSeparator());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (toReturn.contains("recommended to submit application") && qs == ADFTree.QuestionState.qsRejected)
		{
			System.out.println(boolArrToStringList(test));
			System.out.println("Accepting an invalid test");
			try {
				fw.write(boolArrToStringList(test) + System.lineSeparator());
				fw.write("Accepting an invalid test" + System.lineSeparator());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (toReturn.contains("recommended to submit application") && qs == ADFTree.QuestionState.qtAccepted)
		{
			System.out.println(boolArrToStringList(test));
			System.out.println("Accepting an valid test");
		}
		else if (toReturn.contains("recommended not to submit application") && qs == ADFTree.QuestionState.qsRejected)
		{
			System.out.println(boolArrToStringList(test));
			System.out.println("Rejecting an invalid test");
		}
		else
		{
			System.out.println(boolArrToStringList(test));
			System.out.println("ERROR");
			System.out.println("recommended not to submit application:" + toReturn.contains("recommended not to submit application"));
			System.out.println("recommended to submit application:" + toReturn.contains("recommended not to submit application"));
		}
		return toReturn;
	}

}
