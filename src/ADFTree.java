import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.AbstractListModel;

public class ADFTree extends AbstractListModel<ADFNode>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4045335255650888945L;
	public static final String COMMA_DELIMITER = ";";
	public static final String SPACE_DELIMITER = " ";
	public static ArrayList<ADFNode> AllADFNodes = new ArrayList<ADFNode>();
	public enum QuestionState {qsAsking, qsRejected, qtAccepted};
	ArrayList<Integer> FactorIndexes = new ArrayList<Integer>();
	ArrayList<Question> questions = new ArrayList<Question>();
	ArrayList<Integer> questionIndexes = new ArrayList<Integer>();
	String newline = System.getProperty("line.separator");
	
	public void loadADF()
	{
		AllADFNodes.clear();
		ADFNode q;
		InputStream is = getClass().getClassLoader().getResourceAsStream("Admissible.txt");
		try (BufferedReader br = new BufferedReader(new InputStreamReader(is)))
		{
			//Load ADFTree
		    String line;
		    line = br.readLine();//Ignore headers
		    while ((line = br.readLine()) != null)
		    {
		    	if (line.trim().length() > 0)
		    	{
		    		String[] values = line.split(COMMA_DELIMITER);
		    		String[] children = values[2].trim().substring(1, values[2].trim().length() - 1).split(SPACE_DELIMITER);
		    		if (children[0].trim().length() == 0)
		    		{
		    			children = new String[0];
		    		}
		    		//System.out.println(line);
		    		q = new ADFNode(values[1].trim(), values[0].trim(), children, values[3].trim(), values[4].trim(),
		    						values[5].trim(), values[6].trim(), values[7].trim().contentEquals("C"));
		    		AllADFNodes.add(q);
		    	}
		    }
		    //System.out.println("Added Nodes");
		    for (int i = 0; i < AllADFNodes.size(); i++)
		    {
		    	//System.out.println("Calculating " + AllADFNodes.get(i).getID() + "'s children");
		    	AllADFNodes.get(i).calculateChildren(AllADFNodes);
		    	if (AllADFNodes.get(i).getNumOfChildQuestions() > 0)
		    	{
		    		FactorIndexes.add(i);
		    	}
		    }
		    
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Question flow
	    is = getClass().getClassLoader().getResourceAsStream("QuestionFlow.txt");
		try (BufferedReader br = new BufferedReader(new InputStreamReader(is)))
		{
		    String line = br.readLine();//Ignore headers
		    while ((line = br.readLine()) != null)
		    {
		    	if (line.trim().length() > 0)
		    	{
		    		String[] values = line.split(COMMA_DELIMITER);
		    		Question question = new Question(values[1].trim(), values[2].trim(), values[3].trim(), values[0].trim(), values[4].trim());
		    		questions.add(question);
		    	}
		    }
		    questionIndexes.add(0);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Question> getQuestions() {
		return questions;
	}

	public ADFNode findADFFromID(String ADFNodeID)
	{
		for (int i = 0; i < AllADFNodes.size(); i++)
		{
			if (AllADFNodes.get(i).getID().equalsIgnoreCase(ADFNodeID))
			{
				return AllADFNodes.get(i);
			}
		}
		return null;
	}
	
	public int findADFIndexFromID(String ADFNodeID)
	{
		for (int i = 0; i < AllADFNodes.size(); i++)
		{
			if (AllADFNodes.get(i).getID().equalsIgnoreCase(ADFNodeID))
			{
				return i;
			}
		}
		return -1;
	}
	
	public int findQuestionIndexFromID(String ADFNodeID)
	{
		for (int i = 0; i < questions.size(); i++)
		{
			if (questions.get(i).getQuestionID().equalsIgnoreCase(ADFNodeID))
			{
				return i;
			}
		}
		return -1;
	}

	//List model should only return items that have a child question
	@Override
	public ADFNode getElementAt(int arg0) 
	{
		return AllADFNodes.get(FactorIndexes.get(arg0));
	}

	@Override
	public int getSize() 
	{
		int total = 0;
		for (int i = 0; i < AllADFNodes.size(); i++)
		{
			if (AllADFNodes.get(i).getNumOfChildQuestions() > 0)
			{
				total++;
			}
			
		}
		return total;
	}

	public void resetQuestions() 
	{
		for (int i = 0; i < AllADFNodes.size(); i++)
		{
			AllADFNodes.get(i).reset();
		}
		questionIndexes.clear();
		questionIndexes.add(0);
	}

	public void goToPrevQuestion() 
	{
		AllADFNodes.get(findADFIndexFromID(questions.get(questionIndexes.get(questionIndexes.size() - 1)).getADFNodeID())).reset();
		questionIndexes.remove(questionIndexes.size() - 1);		
	}

	//Returns true if at end
	public QuestionState goToNextQuestion(boolean b) 
	{
		int currentQuestionIndex = questionIndexes.get(questionIndexes.size() - 1);	
		//System.out.println(questions.get(questionIndexes.get(questionIndexes.size() - 1)).getQuestionID());
		int adfNodeIndex = findADFIndexFromID(questions.get(currentQuestionIndex).getADFNodeID());
		AllADFNodes.get(adfNodeIndex).setValid(b);
		int nextQuestionIndex = -1;
		if (b)
		{
			if (questions.get(currentQuestionIndex).getTrueNextQuestionID().startsWith("Q"))
			{
				nextQuestionIndex = findQuestionIndexFromID(questions.get(currentQuestionIndex).getTrueNextQuestionID());
			}
			else if (questions.get(currentQuestionIndex).getTrueNextQuestionID().contentEquals("FINISH"))
			{
				return QuestionState.qtAccepted;
			}
			else
			{
				return QuestionState.qsRejected;
			}
		}
		else
		{
			if (questions.get(currentQuestionIndex).getFalseNextQuestionID().startsWith("Q"))
			{
				nextQuestionIndex = findQuestionIndexFromID(questions.get(currentQuestionIndex).getFalseNextQuestionID());
			}
			else if (questions.get(currentQuestionIndex).getFalseNextQuestionID().contentEquals("FINISH"))
			{
				return QuestionState.qtAccepted;
			}
			else
			{
				return QuestionState.qsRejected;
			}
		}
		questionIndexes.add(nextQuestionIndex);
		return QuestionState.qsAsking;
	}

	public String getQuestionText() 
	{
		return findADFFromID(questions.get(questionIndexes.get(questionIndexes.size() - 1)).getADFNodeID()).getText();
	}

	public String getNoteText()
	{
		return findADFFromID(questions.get(questionIndexes.get(questionIndexes.size() - 1)).getADFNodeID()).getTooltipText();
	}

	public boolean isFirstQuestion() 
	{
		return questionIndexes.size() == 1;
	}

	public String resolve() 
	{
		//Resolves all as root node
		return AllADFNodes.get(0).resolve();		
	}

	public String getMainResult() 
	{
		return questions.get(questionIndexes.get(questionIndexes.size() - 1)).getEndReason();
	}
	
	/*public String getChildResults(ADFNode node)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(node.getText());
		for (int i = 0; i < node.getNumOfChildQuestions(); i++)
		{
			getChildResults(node);
		}
		return sb.toString();
	}*/

	public String getReasoning(boolean fullTree) 
	{
		StringBuilder fullReasoning = new StringBuilder();
		if (fullTree)
		{
			ADFNode currNode = AllADFNodes.get(0);
			fullReasoning.append(getChildReasoning(currNode).trim());
		}
		else
		{
			int rejectingNodeIndex = findADFIndexFromID(questions.get(questionIndexes.get(questionIndexes.size() - 1)).getADFNodeID());
			ADFNode currNode = AllADFNodes.get(rejectingNodeIndex);
			currNode = AllADFNodes.get(findADFIndexFromID(currNode.getParentID()));
			
			for (int i = 0; i < currNode.getNumOfChildQuestions(); i++)
			{
				if (currNode.getChildren().get(i).wasAnswered())
				{
					fullReasoning.append(currNode.getChildren().get(i).resolve() + newline);
				}
			}
			
			while(currNode.hasParent())
			{
				fullReasoning.append("So " + currNode.resolve().toLowerCase() + newline);
				currNode = AllADFNodes.get(findADFIndexFromID(currNode.getParentID()));
			}
			fullReasoning.append("Therefore " + currNode.resolve().toLowerCase() + newline);
		}
		return fullReasoning.toString().trim();
	}

	private String getChildReasoning(ADFNode adfNode) 
	{
		ArrayList<ADFNode> childNodes = adfNode.getChildren();
		StringBuilder fullReasoning = new StringBuilder();
		
		switch(adfNode.type)
		{
		case ADFFactor:
			for (int i = 0; i < childNodes.size(); i++)
			{
				fullReasoning.append(getChildReasoning(childNodes.get(i)));
			}
			fullReasoning.append("So " + adfNode.resolve().toLowerCase() + newline);
			break;
		case ADFIssue:
			for (int i = 0; i < childNodes.size(); i++)
			{
				fullReasoning.append(getChildReasoning(childNodes.get(i)));
			}
			fullReasoning.append("So " + adfNode.resolve().toLowerCase() + newline);
			break;
		case ADFQuestion:
			if (adfNode.wasAnswered())
			{
				fullReasoning.append(adfNode.resolve() + newline);
			}
			break;
		case ADFViolation:
			for (int i = 0; i < childNodes.size(); i++)
			{
				fullReasoning.append(getChildReasoning(childNodes.get(i)));
			}
			fullReasoning.append("Therefore " + adfNode.resolve().toLowerCase());
			break;		
		}
		return fullReasoning.toString();
	}
}
