import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JSeparator;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;

public class QuestionWindow {

	public JFrame frame;
	private JLabel lblQuestion;
	private JLabel lblNote;
	private JButton btnPrev;
	private ADFTree adfTree = new ADFTree();
	private enum qwView {qwStartPage, qwQuestion, qwResults};
	//private qwView currentView = qwView.qwStartPage;
	private JLabel lblResult = new JLabel("<html>RESULT</html>");
	private JTextPane txtReasoning = new JTextPane();
	private JPanel pnlResults = new JPanel();
	private JPanel pnlStart = new JPanel();
	private JPanel pnlQuestion = new JPanel();
	private JPanel pnlAnswers = new JPanel();
	
	private Color bkColour = new Color(247,247,247);
	private boolean showAllReasoning = false;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//TitleScreen title = new TitleScreen();
					//title.setVisible(true);
					QuestionWindow window = new QuestionWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public QuestionWindow() 
	{
		adfTree.loadADF();
		initialize();		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setIconImage(new ImageIcon(getClass().getResource("to-do-list_checked2.png")).getImage());
		frame.setTitle("ECHR Application Admissability Checker");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pnlQuestion.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		
		pnlQuestion.setBackground(bkColour);
		frame.getContentPane().add(pnlQuestion, BorderLayout.CENTER);
		pnlQuestion.setLayout(new BorderLayout(0, 0));
		
		lblQuestion = new JLabel("<html>Have you (or every member of the group if an application\n\nis submitted by the group) signed the power of attorney\n\n(page 3 of the applications form)? </html>");
		lblQuestion.setFont(new Font("Dialog", Font.PLAIN, 25));
		pnlQuestion.add(lblQuestion, BorderLayout.NORTH);
		
		lblNote = new JLabel("<html>have the applicant suffered any damage as a result of the alleged violation (damage can include physical damage, arrest of the applicant, arrest of the property, dissolution of an organisation etc) </html>");
		lblNote.setForeground(Color.DARK_GRAY);
		lblNote.setFont(new Font("Dialog", Font.PLAIN, 18));
		lblNote.setVerticalAlignment(SwingConstants.TOP);
		pnlQuestion.add(lblNote, BorderLayout.CENTER);
		
		JPanel pnlNavigation = new JPanel();
		pnlNavigation.setBackground(bkColour);
		pnlQuestion.add(pnlNavigation, BorderLayout.SOUTH);
		
		btnPrev = new JButton("Previous");
		btnPrev.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				goToPrevQuestion();
			}
		});
		pnlNavigation.setLayout(new BorderLayout(0, 0));
		btnPrev.setFont(new Font("Dialog", Font.BOLD, 18));
		pnlNavigation.add(btnPrev, BorderLayout.WEST);
		
		JButton btnYes = new JButton("Yes");
		btnYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				goToNextQuestion(true);
			}
		});
		btnYes.setFont(new Font("Dialog", Font.BOLD, 18));
		FlowLayout flowLayout_1 = (FlowLayout) pnlAnswers.getLayout();
		flowLayout_1.setVgap(0);
		flowLayout_1.setHgap(0);
		pnlAnswers.setBackground(bkColour);
		pnlAnswers.setBorder(new EmptyBorder(0, 0, 0, 0));
		pnlAnswers.add(btnYes);
		
		JButton btnNewButton = new JButton("No");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				goToNextQuestion(false);
			}
		});
		
		Component rigidArea = Box.createRigidArea(new Dimension(5, 20));
		pnlAnswers.add(rigidArea);
		btnNewButton.setFont(new Font("Dialog", Font.BOLD, 18));
		pnlAnswers.add(btnNewButton);
		
		pnlNavigation.add(pnlAnswers, BorderLayout.EAST);
		
		JPanel pnlSeparator = new JPanel();
		pnlSeparator.setBackground(bkColour);
		pnlNavigation.add(pnlSeparator, BorderLayout.NORTH);
		pnlSeparator.setLayout(new BorderLayout(0, 0));
		
		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(0, 0, 255));
		separator.setBackground(new Color(0, 0, 128));
		pnlSeparator.add(separator, BorderLayout.NORTH);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(5, 4));
		pnlSeparator.add(rigidArea_1, BorderLayout.CENTER);
		
		pnlResults.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		
		pnlResults.setBackground(bkColour);
		frame.getContentPane().add(pnlResults, BorderLayout.SOUTH);
		pnlResults.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(bkColour);
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.TRAILING);
		pnlResults.add(panel, BorderLayout.SOUTH);
		
		JButton btnNew = new JButton("New Application");
		btnNew.setFont(new Font("Dialog", Font.BOLD, 18));
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				restart();
				switchView(qwView.qwStartPage);
			}
		});
		
		JButton btnTree = new JButton("Show full explanation");
		btnTree.setVisible(false);
		btnTree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				showAllReasoning = !showAllReasoning;
				showResults(showAllReasoning);
				if (!showAllReasoning)
				{
					btnTree.setText("Show full explanation");
				}
				else
				{
					btnTree.setText("Show decisive explanation");
				}
			}
		});
		btnTree.setFont(new Font("Dialog", Font.BOLD, 18));
		panel.add(btnTree);
		panel.add(btnNew);
		lblResult.setFont(new Font("Dialog", Font.PLAIN, 25));
		lblResult.setBackground(bkColour);
		
		
		pnlResults.add(lblResult, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(bkColour);
		pnlResults.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JLabel lblReasoning = new JLabel("Explanation");
		lblReasoning.setBackground(bkColour);
		lblReasoning.setFont(new Font("Dialog", Font.PLAIN, 20));
		panel_1.add(lblReasoning, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane);
		txtReasoning.setFont(new Font("Dialog", Font.PLAIN, 15));
		
		txtReasoning.setEditable(false);
		txtReasoning.setText("Example\r\nTest\r\n");
		scrollPane.setViewportView(txtReasoning);
		pnlStart.setBackground(bkColour);
		
		
		pnlStart.setBorder(new EmptyBorder(10, 10, 10, 10));
		frame.getContentPane().add(pnlStart, BorderLayout.NORTH);
		pnlStart.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("<html>Welcome to the ECHR Application admissibility checker program</html>");
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 24));
		pnlStart.add(lblNewLabel, BorderLayout.NORTH);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(bkColour);
		pnlStart.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnStart = new JButton("Start");
		btnStart.setFont(new Font("Dialog", Font.BOLD, 18));
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restart();
				switchView(qwView.qwQuestion);
			}
		});
		panel_2.setLayout(new BorderLayout(0, 0));
		panel_2.add(btnStart, BorderLayout.EAST);
		
		JPanel pnlSeparator_1 = new JPanel();
		pnlSeparator_1.setBackground(Color.WHITE);
		panel_2.add(pnlSeparator_1, BorderLayout.NORTH);
		pnlSeparator_1.setLayout(new BorderLayout(0, 0));
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.BLUE);
		separator_1.setBackground(new Color(0, 0, 128));
		pnlSeparator_1.add(separator_1, BorderLayout.NORTH);
		
		Component rigidArea_1_1 = Box.createRigidArea(new Dimension(5, 4));
		pnlSeparator_1.add(rigidArea_1_1, BorderLayout.SOUTH);
		
		JLabel lblNewLabel_1 = new JLabel("<html>This tool guides the user though a number of questions, in order to highlight any possible issues with a ECHR application.\r\n<br/><br/>\r\nThis is a beta program and should NOT be used to inform ANY real application\r\n</html>");
		lblNewLabel_1.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_1.setForeground(Color.DARK_GRAY);
		lblNewLabel_1.setFont(new Font("Dialog", Font.PLAIN, 18));
		pnlStart.add(lblNewLabel_1, BorderLayout.CENTER);
		
		setupQuestion();
		switchView(qwView.qwStartPage);
	}

	protected void restart() 
	{
		adfTree.resetQuestions();
		setupQuestion();
	}

	protected void goToPrevQuestion() 
	{
		adfTree.goToPrevQuestion();
		setupQuestion();
		
	}

	protected void goToNextQuestion(boolean b) 
	{
		switch (adfTree.goToNextQuestion(b))
		{
		case qsAsking:
			setupQuestion();
			break;
		case qsRejected:
			switchView(qwView.qwResults);
			showAllReasoning = false;
			showResults(showAllReasoning);
			break;
		case qtAccepted:
			showAllReasoning = true;
			switchView(qwView.qwResults);
			showResults(showAllReasoning);
			break;
		
		}
	}
	
	private void showResults(boolean accepted) 
	{
		adfTree.resolve();
		lblResult.setText("<html>" + adfTree.getMainResult() + "</html>");
		txtReasoning.setText(adfTree.getReasoning(accepted));
		
	}

	public void setupQuestion()
	{
		lblQuestion.setText("<html>" + adfTree.getQuestionText() + "</html>");
		lblNote.setText("<html>" + adfTree.getNoteText() + "</html>");
		btnPrev.setVisible(!adfTree.isFirstQuestion());
	}
	
	public void switchView(qwView nextView)
	{
		frame.getContentPane().remove(pnlQuestion);
		frame.getContentPane().remove(pnlStart);
		frame.getContentPane().remove(pnlResults);
		pnlQuestion.setVisible(nextView == qwView.qwQuestion);
		pnlStart.setVisible(nextView == qwView.qwStartPage);
		pnlResults.setVisible(nextView == qwView.qwResults);
		switch(nextView)
		{
		case qwQuestion:
			frame.getContentPane().add(pnlQuestion, BorderLayout.CENTER);
			break;
		case qwResults:
			frame.getContentPane().add(pnlResults, BorderLayout.CENTER);
			break;
		default: //If undefined go to the startPage;
		case qwStartPage:
			frame.getContentPane().add(pnlStart, BorderLayout.CENTER);
			break;
		
		}
		//currentView = nextView;
		frame.revalidate();
		frame.repaint();
	}

}
