import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class ApplicationWindow {

	private ADFTree mainclass = new ADFTree();
	private enum ViewMode {vmQuestions, vmResults};
	private HashMap<ViewMode, Integer> mpDividerPos = new HashMap<ViewMode, Integer>();
	private ViewMode vmView = ViewMode.vmQuestions;
	private Properties settings = new Properties();
	
	//GUI Components
	private JFrame frmArticleAdf;
	private JSplitPane slpMainView = new JSplitPane();
	private JButton btnCopy = new JButton();
	private JButton btnFinish = new JButton();
	
	private JPanel pnlTextResults = new JPanel();
	private JScrollPane scrResults = new JScrollPane();
	private JTextPane txtResults = new JTextPane();
	private JLabel lblOverallResult = new JLabel("", SwingConstants.CENTER);
	private JTree treResults = new JTree();
	
	private JPanel pnlAllQuestions = new JPanel();	
	private JPanel pnlCheckBoxs = new JPanel();
	private JScrollPane scrTreeResults = new JScrollPane();
	private JScrollPane scrQuestions = new JScrollPane();
	
	private JPanel pnlQuestionSelection = new JPanel();
	private JList<ADFNode> lstQuestions = new JList<ADFNode>(mainclass);
	private JPanel questionPanel = new JPanel();
	private BoxLayout blQuestions = new BoxLayout(pnlCheckBoxs, BoxLayout.Y_AXIS);
	private JPanel questionNavPanel = new JPanel();
	private JButton btnPrev = new JButton();
	private JButton btnNext = new JButton();
	
	private Color bkColour = new Color(253,251,251);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApplicationWindow window = new ApplicationWindow();
					window.frmArticleAdf.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Set view type
	 */
	public void setViewMode(ViewMode view)
	{
		mpDividerPos.put(vmView, slpMainView.getDividerLocation());
		vmView = view;
		switch(vmView)
		{
		case vmQuestions:
			slpMainView.setLeftComponent(pnlQuestionSelection);
			slpMainView.setRightComponent(pnlAllQuestions);
			break;
		case vmResults:
			slpMainView.setLeftComponent(scrTreeResults);
			slpMainView.setRightComponent(pnlTextResults);
			break;
		default:
			break;
		
		}
		btnCopy.setOpaque(false);
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
				cb.setContents(new StringSelection(txtResults.getText()), null);
			}
		});
		btnFinish.setOpaque(false);
		btnFinish.setVisible(vmView == ViewMode.vmQuestions);
		btnCopy.setVisible(vmView == ViewMode.vmResults);
		slpMainView.setBorder(null);
		slpMainView.setDividerLocation(mpDividerPos.get(vmView));
	}
	
	public void resetQuestions()
	{
		mainclass.resetQuestions();
	}
	/**
	 * Create the application.
	 */
	public ApplicationWindow() {
		UIManager.put("Tree.leafIcon", new ImageIcon(getClass().getResource("comments_alert.png")));
		UIManager.put("Tree.openIcon", new ImageIcon(getClass().getResource("comments_report.png")));
		UIManager.put("Tree.closedIcon", new ImageIcon(getClass().getResource("comments_report.png")));
		//boolean loaded = false;
		/*while (!loaded)
		{
			try {
				FileInputStream in = new FileInputStream("settings");
				settings.load(in);
				mainclass.loadADF(settings.getProperty("ADFLocation","ADF.txt"));
				loaded = true;
			} catch (Exception e) {
				DlgSettings setSettings = new DlgSettings(settings, mainclass);
				setSettings.setVisible(true);
			}
		}*/
		mainclass.loadADF();
		mpDividerPos.put(ViewMode.vmQuestions, 350);
		mpDividerPos.put(ViewMode.vmResults, 350);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmArticleAdf = new JFrame();
		frmArticleAdf.getContentPane().setFont(new Font("Dialog", Font.PLAIN, 18));
		frmArticleAdf.setSize(new Dimension(1200, 800));
		frmArticleAdf.setIconImage(new ImageIcon(getClass().getResource("to-do-list_checked2.png")).getImage());
		frmArticleAdf.getContentPane().setBackground(Color.GRAY);
		frmArticleAdf.setTitle("ECHR Application Admissability Checker");
		frmArticleAdf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmArticleAdf.getContentPane().add(slpMainView, BorderLayout.CENTER);
		
		//Select Questions Panel.
		pnlQuestionSelection.setLayout(new BorderLayout());
		pnlQuestionSelection.setBorder(BorderFactory.createEmptyBorder(6,0,0,0));
		JLabel lblQuestionSelection = new JLabel("Question Group", SwingConstants.CENTER);
		lblQuestionSelection.setFont(new Font(lblQuestionSelection.getFont().getName(), Font.PLAIN, 25));
		pnlQuestionSelection.add(lblQuestionSelection, BorderLayout.NORTH);
		pnlQuestionSelection.setBackground(bkColour);
		
		lstQuestions.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting())
				{
					updateQuestions();
				}
			}
		});
		//lstQuestions.setCellRenderer(mainclass);
		lstQuestions.setBackground(bkColour);
		lstQuestions.setFont(new Font(lblQuestionSelection.getFont().getName(), Font.PLAIN, 18));
		//lstQuestions.setPrototypeCellValue(new ADFNode("OneIncredibleyLongUnbrokenSentence", "", new String[]{}, "", "", "", "", false));
		//lstQuestions.setFixedCellHeight(23);
		//lstQuestions.setFixedCellWidth(610);
		scrQuestions.setViewportView(lstQuestions);
		scrQuestions.setBorder(new MatteBorder(1,0,0,0, Color.BLUE));
		pnlQuestionSelection.add(scrQuestions, BorderLayout.CENTER);
		
		txtResults.setSize(new Dimension(400, 0));
		txtResults.setMinimumSize(new Dimension(400, 10));
		txtResults.setEditable(false);
		scrResults.setViewportView(txtResults);
		scrResults.setBorder(new MatteBorder(1, 0, 0, 0, Color.BLUE));
		txtResults.setOpaque(false);
		
		treResults.setBackground(bkColour);
		treResults.setBorder(new EmptyBorder(0, 0, 0, 0));
		scrTreeResults.setViewportView(treResults);
		scrTreeResults.setBorder(new EmptyBorder(0,0,0,0));
		slpMainView.setDividerSize(8);
		
		
		questionPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		
		/*question navigation panel*/
		questionNavPanel.setBackground(bkColour);
		questionNavPanel.add(btnPrev);
		questionNavPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		btnPrev.setText("Previous");
		btnPrev.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnPrev.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GoToPrev();				
			}
		});
		questionNavPanel.add(btnNext);
		btnNext.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnNext.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GoToNext();				
			}
		});
		
		pnlAllQuestions.setBorder(BorderFactory.createEmptyBorder(6,6,0,6));
		pnlAllQuestions.setLayout(new BorderLayout(3,3));
		pnlAllQuestions.setBackground(bkColour);
		
		pnlCheckBoxs.setLayout(blQuestions);
		pnlCheckBoxs.setBorder(BorderFactory.createEmptyBorder(6,6,0,6));
		pnlCheckBoxs.setBackground(bkColour);
		
		pnlTextResults.setBorder(BorderFactory.createEmptyBorder(6,0,0,0));
		pnlTextResults.setLayout(new BorderLayout());
		lblOverallResult.setFont(new Font(lblOverallResult.getFont().getName(), Font.PLAIN, 25));
		pnlTextResults.add(lblOverallResult, BorderLayout.NORTH);
		pnlTextResults.add(scrResults, BorderLayout.CENTER);
		pnlTextResults.setBackground(bkColour);
		
		
		/**
		 * Toolbar setup
		 */
		JGradientToolbar toolBar = new JGradientToolbar();
		toolBar.setRollover(true);
		toolBar.setAlignmentY(Component.CENTER_ALIGNMENT);
		toolBar.setFloatable(false);
		frmArticleAdf.getContentPane().add(toolBar, BorderLayout.NORTH);
		
		JButton btnNew = new JButton();
		btnNew.setOpaque(false);
		btnNew.setToolTipText("New");
		btnNew.setIcon(new ImageIcon(getClass().getResource("document-lined-pen.png")));
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				resetQuestions();
				setViewMode(ViewMode.vmQuestions);
				lstQuestions.setSelectedIndex(0);
				updateQuestions();
			}
		});
		toolBar.add(btnNew);
		
		btnFinish.setToolTipText("Finish");
		btnFinish.setIcon(new ImageIcon(getClass().getResource("to-do-list_checked3.png")));
		toolBar.add(btnFinish);
		
		btnCopy.setToolTipText("Copy Results");
		btnCopy.setIcon(new ImageIcon(getClass().getResource("copy-document.png")));
		toolBar.add(btnCopy);
		
		JSeparator separator = new JSeparator(SwingConstants.VERTICAL);
		separator.setVisible(false);
		separator.setMaximumSize(new Dimension(6, Integer.MAX_VALUE) );
		toolBar.add(separator);
		
		JButton btnSettings = new JButton();
		btnSettings.setVisible(false);
		btnSettings.setOpaque(false);
		btnSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DlgSettings dlgsettings = new DlgSettings(settings, mainclass);
				dlgsettings.setVisible(true);
			}
		});
		btnSettings.setToolTipText("Settings");
		btnSettings.setIcon(new ImageIcon(getClass().getResource("preferences.png")));
		toolBar.add(btnSettings);
		
		btnFinish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				finishedQuestions();
			}
		});
		setViewMode(ViewMode.vmQuestions);
		lstQuestions.setSelectedIndex(0);		
	}

	private DefaultTreeModel newAnswers()
	{
		for (int i = 0; i < ADFTree.AllADFNodes.size(); i++)
		{
			ADFTree.AllADFNodes.get(i).setDirty();
		}
		DefaultMutableTreeNode rootNode = ADFTree.AllADFNodes.get(0).getTreeNode();
		return new DefaultTreeModel(rootNode);
	}
	
	private void GoToPrev()
	{
		lstQuestions.setSelectedIndex(lstQuestions.getSelectedIndex() - 1);
		updateQuestions();
	}
	
	private void GoToNext()
	{
		if (lstQuestions.getSelectedIndex() < mainclass.getSize() - 1)
		{
			lstQuestions.setSelectedIndex(lstQuestions.getSelectedIndex() + 1);
			updateQuestions();
		}
		else
		{
			finishedQuestions();
		}
	}
	
	private void finishedQuestions()
	{
		setViewMode(ViewMode.vmResults);		
		DefaultTreeModel results = newAnswers();
		treResults.setModel(results);
		
		StringBuilder fullString = new StringBuilder();
		lblOverallResult.setText(ADFTree.AllADFNodes.get(0).resolve());
		for (int i = 0; i < ADFTree.AllADFNodes.size(); i++)
		{
			fullString.append(ADFTree.AllADFNodes.get(i).resolve() + System.lineSeparator());
			
		}
		txtResults.setText(fullString.toString());
	}
	
	public void updateQuestions()
	{
		btnPrev.setVisible(lstQuestions.getSelectedIndex() > 0);
		pnlAllQuestions.removeAll();
		JLabel lblAllQuestion = new JLabel(lstQuestions.getSelectedValue().getText() + " Questions", SwingConstants.CENTER);
		lblAllQuestion.setFont(new Font(lblAllQuestion.getFont().getName(), Font.PLAIN, 25));
		pnlAllQuestions.add(lblAllQuestion, BorderLayout.PAGE_START);
		JSeparator separator = new JSeparator(SwingConstants.HORIZONTAL);
		separator.setMaximumSize(new Dimension(Integer.MAX_VALUE, 1) );
		pnlCheckBoxs.removeAll();
		pnlCheckBoxs.add(separator);
		for (int i = 0; i < mainclass.getElementAt(lstQuestions.getSelectedIndex()).getNumOfChildQuestions(); i++)
		{
			addQuestion(i);
		}
		if (lstQuestions.getSelectedIndex() < mainclass.getSize() - 1)
		{
			btnNext.setText("Next");
		}
		else
		{
			btnNext.setText("Finish");
		}
		pnlAllQuestions.add(pnlCheckBoxs, BorderLayout.CENTER);
		pnlAllQuestions.add(questionNavPanel, BorderLayout.PAGE_END);
		pnlAllQuestions.invalidate();
		pnlAllQuestions.repaint();
		pnlAllQuestions.revalidate();
	}
	
	/**
	 * Add New Question Panel
	 */
	private void addQuestion(int questionNumber)
	{
		/*JPanel questionPanel = new JPanel();
		questionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		*/
		JCheckBox ck = new JCheckBox(mainclass.getElementAt(lstQuestions.getSelectedIndex()).getQuestionChild(questionNumber).getText());
		ck.setSelected(mainclass.getElementAt(lstQuestions.getSelectedIndex()).getQuestionChild(questionNumber).isValid());
		ck.setFont(new Font(ck.getFont().getName(), Font.PLAIN, 18));
		ck.addActionListener(new CheckBoxListener(mainclass.getElementAt(lstQuestions.getSelectedIndex()).getQuestionChild(questionNumber)));
		ck.setToolTipText(mainclass.getElementAt(lstQuestions.getSelectedIndex()).getQuestionChild(questionNumber).getTooltipText());
		ck.setOpaque(false);
		pnlCheckBoxs.add(ck);
		
		/*JLabel infoLabel = new JLabel();
		infoLabel.setPreferredSize(new Dimension(32, 32));
		infoLabel.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("information.png")).getImage().getScaledInstance(32, 32, Image.SCALE_DEFAULT)));
		questionPanel.add(infoLabel);*/
		
		//questionPanel.revalidate();
		//pnlAllQuestions.add(questionPanel);
	}
}
