import java.util.ArrayList;
import java.util.Stack;

import javax.swing.tree.DefaultMutableTreeNode;

public class ADFNode
{
	public static enum ADFType {ADFViolation, ADFIssue, ADFFactor, ADFQuestion};
	public ADFType type;
	private String text;
	private String ID;
	private String parentID = "";
	private String validString;
	private String invalidString;
	private String tooltipText;
	private boolean defaultValue;
	private boolean valid;
	private boolean dirty = true;
	private boolean answered = false;
	private int numOfChildQuestions = 0;
	private ArrayList<ADFNode> children = new ArrayList<ADFNode>();
	private String[] childString;
	private String resolveLogic;
	
	public ADFNode(String text, String iD, String[] childrenStr, String resolve, String validText, String invalidText, String tooltipText, Boolean defaultValue) 
	{
		this.text = text;
		validString = validText;
		invalidString = invalidText;
		this.tooltipText = tooltipText;
		this.defaultValue = defaultValue;
		this.valid = defaultValue;
		ID = iD;
		if (ID.contains("Q"))
		{
			type = ADFType.ADFQuestion;
		}
		else if (ID.contains("F"))
		{
			type = ADFType.ADFFactor;
		}
		else if (ID.contains("I"))
		{
			type = ADFType.ADFIssue;
		}
		else
		{
			type = ADFType.ADFViolation;
		}
		childString = childrenStr;
		if (childString.length > 0)
		{
			resolveLogic = resolve.substring(resolve.indexOf("ACCEPT IF")+9, resolve.lastIndexOf("REJECT OTHERWISE")).trim();
		}
	}
	
	public void setDirty()
	{
		dirty = true;
	}
	
	public void addChild(ADFNode child)
	{
		children.add(child);
	}
	
	public void setParentID(String parentID)
	{
		this.parentID = parentID;
	}
	
	public ArrayList<ADFNode> getChildren()
	{
		return children;
	}
	
	public int getNumOfChildQuestions()
	{
		return numOfChildQuestions;
	}
	
	public void setValid(boolean isValid)
	{
		answered = true;
		valid = isValid;
	}
	
	public String resolve()
	{
		if (isValid())
		{
			return validString;
		}
		else
		{
			return invalidString;
		}
	}
	
	public boolean isValid()
	{
		if (!dirty)
		{
			return valid;
		}
		else if (type == ADFType.ADFQuestion)
		{
			return valid;
		}
		else
		{
			boolean satifiable = false; //Left hand side
			satifiable = resolver(resolveLogic);
			valid = satifiable;
			dirty = false;
			return satifiable;
		}
	}
	
	private boolean resolver(String expression)
	{
		//Replace brackets
		Stack<Integer> leftBrackets = new Stack<Integer>();
		for (int i = 0; i < expression.length(); i++)
		{
			if (expression.charAt(i) == '(')
			{
				leftBrackets.push(i);
			}
			else if (expression.charAt(i) == ')')
			{
				int start = leftBrackets.pop();
				int end = i + 1;
				if (!(start == 0 && end == expression.length()))
				{
					boolean result = resolver(expression.substring(start, end));
					expression = expression.replace(expression.substring(start, end), Boolean.toString(result));
					i = start;
				}
			}
		}
		//replace nodeids with values
		//System.out.println("Resolving " + expression);
		String[] statement = expression.replace("(", "").replace(")", "").split(ADFTree.SPACE_DELIMITER);
		boolean currentResult = false;
		boolean nextAND = false;
		for (int i = 0; i < statement.length; i++)
		{
			if (statement[i].equals("AND"))
			{
				nextAND = true;
			}
			else if (statement[i].equals("OR"))
			{
				nextAND = false;
			}
			else
			{
				boolean current = false;
				if (statement[i].equals("true"))
				{
					current = true;
				}
				else if (!statement[i].equals("false"))
				{
					if (statement[i].charAt(0) == '!')
					{
						current = !ADFNode.findADFNode(statement[i].replace("!", ""), ADFTree.AllADFNodes).isValid();
						
					}
					else
					{
						current = ADFNode.findADFNode(statement[i], ADFTree.AllADFNodes).isValid();
					}
				}
				if (nextAND)
				{
					currentResult = currentResult && current;
				}
				else
				{
					currentResult = currentResult || current;
				}
			}
		}
		return currentResult;
	}
	
	public DefaultMutableTreeNode getTreeNode()
	{
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(resolve());
		for (int i = 0; i < children.size(); i++)
		{
			node.add(children.get(i).getTreeNode());
		}
		return node;
	}
	
	public void calculateChildren(ArrayList<ADFNode> allNodes)
	{
		for(int i = 0; i < childString.length; i++)
		{
			String childID = childString[i];
			children.add(findADFNode(childID, allNodes));
			findADFNode(childID, allNodes).setParentID(ID);
			if (children.get(i).getType() == ADFType.ADFQuestion)
			{
				numOfChildQuestions++;
			}
		}
	}
	
	@Override
	public String toString() {
		return text;
	}
	public String getText() {
		return text;
	}
	public String getID() {
		return ID;
	}
	
	public ADFType getType()
	{
		return type;
	}
	
	public ADFNode getQuestionChild(int num)
	{
		int currentNum = 0;
		for(int i = 0; i < children.size(); i++)
		{
			if ((children.get(i).getType() == ADFType.ADFQuestion) && (currentNum == num))
			{
				return children.get(i);
			}
			
			if (children.get(i).getType() == ADFType.ADFQuestion)
			{
				currentNum++;
			}
		}
		return null;
	}
	
	public static ADFNode findADFNode(String ID, ArrayList<ADFNode> allNodes)
	{
		for(int i = 0; i < allNodes.size(); i++)
		{
			if (allNodes.get(i).ID.equals(ID))
			{
				return allNodes.get(i);
			}
		}
		System.out.println("Could not find node " + ID);
		return null; //Possibly return error here
	}

	public String getValidString() {
		return validString;
	}

	public String getInvalidString() {
		return invalidString;
	}

	public boolean isDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(boolean defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getTooltipText() {
		return tooltipText;
	}

	public void setTooltipText(String tooltipText) {
		this.tooltipText = tooltipText;
	}

	public void reset() 
	{
		dirty = true;
		valid = defaultValue;
		answered = false;
	}

	public String getParentID()
	{
		return parentID;
	}

	public boolean hasParent() 
	{
		return !parentID.isEmpty();
	}
	
	public boolean wasAnswered()
	{
		return answered;
	}
}
