import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JToolBar;

public class JGradientToolbar extends JToolBar 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3880461677718761783L;

	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D)g.create();
	
		// Apply vertical gradient
		g2.setPaint(new GradientPaint(0, 0,new Color(194,233,251), 0, getHeight(), new Color(161,196,253)));
		g2.fillRect(0, 0, getWidth(), getHeight());
		
		// Dipose of copy
		g2.dispose();
	}
}
