class PossibleOutput implements Comparable<PossibleOutput>
{
	public String run;
	public String explanation;
	public PossibleOutput(String run, String explanation)
	{
		this.run = run;
		this.explanation = explanation;
	}
	
	@Override
	public String toString()
	{
		return explanation;
	}

	@Override
	public int compareTo(PossibleOutput arg0) {
		// TODO Auto-generated method stub
		return explanation.compareTo(arg0.explanation);
	}
}
