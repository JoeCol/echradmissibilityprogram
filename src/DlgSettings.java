import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class DlgSettings extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3391316570025372620L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtADFLocation;
	private ADFTree program;
	private Properties allSettings;

	/**
	 * Create the dialog.
	 * @param applicationWindow 
	 */
	public DlgSettings(Properties settings, ADFTree mainclass) {
		allSettings = settings;
		program = mainclass;
		setIconImage(new ImageIcon(getClass().getResource("to-do-list_checked2.png")).getImage());
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Settings");
		setBounds(100, 100, 450, 155);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{132, 114, 62, 0};
		gbl_contentPanel.rowHeights = new int[]{24, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblModelFileLocation = new JLabel("Model File Location");
			GridBagConstraints gbc_lblModelFileLocation = new GridBagConstraints();
			gbc_lblModelFileLocation.insets = new Insets(0, 0, 5, 5);
			gbc_lblModelFileLocation.anchor = GridBagConstraints.EAST;
			gbc_lblModelFileLocation.gridx = 0;
			gbc_lblModelFileLocation.gridy = 0;
			contentPanel.add(lblModelFileLocation, gbc_lblModelFileLocation);
		}
		{
			txtADFLocation = new JTextField();
			GridBagConstraints gbc_txtADFLocation = new GridBagConstraints();
			gbc_txtADFLocation.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtADFLocation.insets = new Insets(0, 0, 5, 5);
			gbc_txtADFLocation.gridx = 1;
			gbc_txtADFLocation.gridy = 0;
			contentPanel.add(txtADFLocation, gbc_txtADFLocation);
			txtADFLocation.setColumns(10);
		}
		{
			JButton btnLoad = new JButton("Change");
			btnLoad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					JFileChooser fc = new JFileChooser();
					fc.setDialogTitle("Set ADF File Location");
					fc.setCurrentDirectory(new File(txtADFLocation.getText()));
					if (fc.showDialog(btnLoad, "Choose") == JFileChooser.APPROVE_OPTION)
					{
						txtADFLocation.setText(fc.getSelectedFile().getAbsolutePath());
					}
				}
			});
			GridBagConstraints gbc_btnLoad = new GridBagConstraints();
			gbc_btnLoad.insets = new Insets(0, 0, 5, 0);
			gbc_btnLoad.anchor = GridBagConstraints.NORTHWEST;
			gbc_btnLoad.gridx = 2;
			gbc_btnLoad.gridy = 0;
			contentPanel.add(btnLoad, gbc_btnLoad);
		}
		{
			JLabel lblReloadModelFile = new JLabel("Reload Model File");
			GridBagConstraints gbc_lblReloadModelFile = new GridBagConstraints();
			gbc_lblReloadModelFile.anchor = GridBagConstraints.EAST;
			gbc_lblReloadModelFile.insets = new Insets(0, 0, 0, 5);
			gbc_lblReloadModelFile.gridx = 0;
			gbc_lblReloadModelFile.gridy = 1;
			contentPanel.add(lblReloadModelFile, gbc_lblReloadModelFile);
		}
		{
			JButton btnReload = new JButton("Reload");
			btnReload.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					program.loadADF();
					JOptionPane.showMessageDialog(contentPanel, "ADF Reloaded", "Success", JOptionPane.INFORMATION_MESSAGE);
				}
			});
			GridBagConstraints gbc_btnReload = new GridBagConstraints();
			gbc_btnReload.gridx = 2;
			gbc_btnReload.gridy = 1;
			contentPanel.add(btnReload, gbc_btnReload);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						allSettings.put("ADFLocation", txtADFLocation.getText());
						try {
							FileOutputStream out = new FileOutputStream("settings");
							allSettings.store(out, "");
							setVisible(false);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		txtADFLocation.setText(settings.getProperty("ADFLocation","ADF.txt"));
	}

}
