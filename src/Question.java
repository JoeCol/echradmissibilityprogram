
public class Question
{
	private String ADFNodeID;
	private String trueNextQuestionID;
	private String falseNextQuestionID;
	private String questionID;
	private String endReason;
	private boolean setAnswer;
	
	public Question(String aDFNodeID, String trueNextQuestionID, String falseNextQuestionID, String questionID, String endReason) {
		super();
		ADFNodeID = aDFNodeID;
		this.trueNextQuestionID = trueNextQuestionID;
		this.falseNextQuestionID = falseNextQuestionID;
		this.questionID = questionID;
		this.endReason = endReason;
	}

	public String getADFNodeID() {
		return ADFNodeID;
	}

	public String getTrueNextQuestionID() {
		setAnswer = true;
		return trueNextQuestionID;
	}

	public String getFalseNextQuestionID() {
		setAnswer = false;
		return falseNextQuestionID;
	}

	public String getQuestionID() {
		return questionID;
	}

	public String getEndReason() {
		if (endReason.startsWith("["))
		{
			int start = setAnswer ? 1 : endReason.indexOf(":") + 1;
			int end = setAnswer ? endReason.indexOf(":") - 1 : endReason.indexOf("]");
			String splitEndReason = endReason.substring(start, end);
			return splitEndReason;
		}
		else
		{
			return endReason;
		}
	}
}
