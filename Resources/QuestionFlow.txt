QuestionID;NodeID;NextIfTrue;NextIfFalse;endReason
Q1;I2F1Q1;Q2;Q10; 
Q2;I1F1Q1;Q3;Q9; 
Q3;I1F1Q4;Q4;Q7; 
Q4;I1F1Q3;Q5;Q6; 
Q5;I1F1Q2;Q19;Q19; 
Q6;I1F1Q2;Q19;NULL;The application will not be registered because it does not comply with Rule 47 of the Rules of Court. You or your legal representative will be required to sign the application. 
Q7;I1F1Q2;NULL;NULL;[The application will not be registered because it does not comply with Rule 47 of the Rules of Court. You will be required to sign the power of attorney.:Your legal representative must sign the application for them to represent you in court]
Q8;I1F1Q3;Q19;NULL;The application will not be registered because it does not comply with Rule 47 of the Rules of Court. You or your legal representative will be required to sign the application.
Q9;I1F1Q3;Q19;NULL;The application will not be registered because it does not comply with Rule 47 of the Rules of Court. You or your legal representative will be required to sign the application.
Q10;I2F1Q2;Q11;NULL;The application will be inadmissible because according to Article 34 of the European Convention the Court accepts individual applications from any person; non-governmental organisation or group of persons.
Q11;I1F1Q8;Q12;NULL;The application will not be registered because it does not comply with Rule 47 of the Rules of Court. You will be required to fill out page 4 of the application. 
Q12;I1F1Q1;Q14;Q13; 
Q13;I1F1Q7;Q19;NULL;The application will not be registered because it does not comply with Rule 47 of the Rules of Court. Your director will be required to sign the application. 
Q14;I1F1Q5;Q17;NULL;The application will need to be signed on page 4 by the legal representative for them to represent you in court.
Q15;I1F1Q6;NULL;Q16; Your director will be required to sign the application. 
Q16;I1F1Q7;Q19;NULL;The application will not be registered because it does not comply with Rule 47 of the Rules of Court. Your director will be required to sign the application.  
Q17;I1F1Q7;Q19;Q18; 
Q18;I1F1Q6;Q19;NULL;The application will not be registered because it does not comply with Rule 47 of the Rules of Court. Your director or legal representative will be required to sign the application. 
Q19;I2F2Q1;Q20;Q21; 
Q20;I2F2Q3;NULL;Q25;The application will be declared inadmissible because only victims of alleged violations can apply to the European Court. 
Q21;I2F2Q2;Q22;Q23; 
Q22;I2F2Q3;NULL;Q25;The application will be declared inadmissible because only victims of alleged violations can apply to the European Court. 
Q23;I2F2Q4;Q24;NULL;The application will be declared inadmissible because only victims of alleged violations can apply to the European Court. 
Q24;I2F2Q3;NULL;Q25;The application will be declared inadmissible because only victims of alleged violations can apply to the European Court.  
Q25;I2F3F1F1F1Q1;Q26;Q30; 
Q26;I2F3F1F1Q1;Q27;Q28; 
Q27;I2F3F1F1Q2;Q30;NULL;If the applicant did not make any attempts to complain about their rights violation in domestic proceedings then their application will be declared inadmissible.
Q28;I2F3F1F1F1Q2;Q29;Q30; 
Q29;I2F3F1F1Q3;Q30;NULL;If the applicant did not make a reasonable attempt to exhaust domestic remedies then the Court will declare such application inadmissible. 
Q30;I2F3F2Q1;Q35;Q31; 
Q31;I2F3F2F1Q2;Q32;Q33; 
Q32;I2F3F2F1Q1;NULL;NULL;[You need to complain to the national authorities first and if unsuccessful then bring your claim to the European Court. At the moment the application is inadmissible.:The application will be declared inadmissible as it was not submitted with the 6 months period.] 
Q33;I2F3F2F1Q1;Q35;Q34; 
Q34;I2F3F2F1Q3;Q35;NULL;The application will be declared inadmissible as it was not submitted with the 6 months period.  
Q35;I2F3F3Q1;Q36;NULL;The application is inadmissible because the application form should contain information about the applicant.  
Q36;I2F3F4F1Q1;Q37;Q39; 
Q37;I2F3F4F1Q2;Q38;Q39; 
Q38;I2F3F4F1Q3;Q39;NULL;The application will be declared inadmissible due to the fact that you cannot submit substantively the same applications twice.  
Q39;I2F3F4F2Q1;Q40;Q42; 
Q40;I2F3F4F2Q2;Q41;Q42; 
Q41;I2F3F4F2Q3;NULL;Q42;The application will be declared inadmissible because it was submitted to another international body.
Q42;I2F3F5F1Q1;NULL;Q43;The application is inadmissible for abuse of the right of petition 
Q43;I2F3F5F1Q2;NULL;Q44;The application is inadmissible for abuse of the right of petition 
Q44;I2F3F5F1Q3;NULL;Q45;The application is inadmissible for abuse of the right of petition 
Q45;I2F3F5F1Q4;NULL;Q46;The application is inadmissible for abuse of the right of petition 
Q46;I2F3F5F2Q1;Q47;Q48; 
Q47;I2F3F5F2Q2;NULL;Q48;The application is inadmissible for abuse of the right of petition 
Q48;I2F3F5F3Q1;NULL;Q49;It is highly likely that the Court will declare your new application manifestly ill-founded too.
Q49;I2F4F1Q1;Q50;NULL;The application is inadmissible the European Court of Human Rights can only consider alleged violation committed by the Contracting Parties to the European Convention on Human Rights.
Q50;I1Q1;Q51;NULL;The application might not be registered as it does not comply with rule 47 of the Rules of Court. 
Q51;I2F4F1Q2;Q52;Q53; 
Q52;I2F4F1Q3;Q53;NULL;The application will be declared inadmissible as the Court can only review cases of violation of rights ratified by the Contracting Parties.
Q53;I2F4F2Q1;Q54;Q55; 
Q54;I1Q2;Q60;NULL;The application might not be registered as it does not comply with rule 47 of the Rules of Court.
Q55;I2F4F2Q2;Q56;Q57; 
Q56;I1Q2;Q60;NULL;The application might not be registered as it does not comply with rule 47 of the Rules of Court.
Q57;I2F4F2Q3;Q58;Q59; 
Q58;I1Q2;Q60;NULL;The application might not be registered as it does not comply with rule 47 of the Rules of Court.
Q59;I2F4F2Q4;Q60;NULL;The application is likely to be declared inadmissible. The violation must occur under the jurisdiction of one of the Contracting Parties to the Convention
Q60;I2F5F1Q1;Q61;Q63; 
Q61;I2F5F1Q2;Q62;NULL;It is likely that the application will be declared inadmissible.
Q62;I2F5F1Q3;Q63;NULL;It is likely that the application will be declared inadmissible. 
Q63;I2F5F2F1Q1;NULL;Q64;It is likely that the application will be declared inadmissible.
Q64;I2F5F2F1Q2;Q65;NULL;It is likely that the application will be declared inadmissible.
Q65;I2F5F2F2Q1;Q67;Q66; 
Q66;I2F5F2F2Q2;Q67;NULL;It is likely that the application will be declared inadmissible. 
Q67;I1F2Q1;Q68;NULL;It is likely that the application form will not be registered due to its failure to comply with Rule 47 of the Rules of Court.
Q68;I1F2Q2;Q69;NULL;It is likely that the application form will not be registered due to its failure to comply with Rule 47 of the Rules of Court.
Q69;I2F5F2F3Q1;Q71;Q70; 
Q70;I2F5F2F3Q2;Q71;NULL;It is likely that the application will be declared inadmissible.
Q71;I2F5F3Q1;FINISH;Q72;It is possible that the application will be declared admissible. 
Q72;I2F5F3Q2;FINISH;Q73;It is possible that the application will be declared admissible. 
Q73;I2F5F3F1Q2;Q74;NULL;It is possible that the application will be declared admissible.
Q74;I2F5F3F1Q1;FINISH;NULL;[It is possible that the application will be declared admissible:It is likely that the application will be declared inadmissible.] 